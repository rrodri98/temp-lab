# -*- coding: utf-8 -*-
'''@file        aaron_mcp9808.py
@brief          Contains class mcp9808 class and methods
@author         Aaron Tran
@author         Rebecca Rodriguez
@date           5/11/21
'''
from pyb import ADCAll
from pyb import I2C
import utime

#SLAVE ADDRESS: 0b0011000
## Array to store the ambient temperature data
ambient_temp_list = []
## Array to store the internal temperature of the Nucleo
internal_temp_list = []

class TaskInternalTemp:
    '''
    
    '''
    
    def __init__(self):
        '''
        Creates an internal temperature task object.
        '''
        #self.adcall = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
        ##@brief Temperature reading from Nucleo
        self.temp = 0;
        pass
        
    def celsius(self):
        '''
        Returns internal temperature in degrees Celsius
        '''
        self.adcall.read_vref()
        self.temp = self.adcall.read_core_temp()
        return self.temp
    
    def fahrenheit(self):
        '''
        Returns internal temperature in degrees Fahrenheit
        '''
        self.temp = self.adcall.read_core_temp()
        return self.temp * (9/5) + 32     

class mcp9808:
    '''@brief Reads temperature data from mcp9808 sensor
    @details Handles I2C communication between microcontroller and mcp9808. Contains methods for reading ambient temperature, checking if the sensor is properly attached,
             and converting temperature readings to rather degrees fahrenheit or degrees celsius
    '''
    def __init__(self, i2c, address):
        '''@brief Initializes class
        @param i2c an object of the I2C, should be configured as MASTER along with whatever SLA/SCL bus is necessary
        @param address 7-bit address of I2C bus
        '''
        ##@brief class attribute of the i2c parameter
        self.i2c = i2c
        ##@brief class attribute of the address parameter
        self.address = address
                
    def read_ambient(self):
        '''@brief Retrieves ambient temperature readings from the sensor in degrees Celsius
        '''
        ##@brief Temperature vector, where first position is the numerical value, and the second position is the units (C or F)
        self.Temperature = [0,0]
        ##@brief timestamp for periodically running the read_ambient() function
        self.now = utime.ticks_ms()    
        while True:
            if utime.ticks_diff(utime.ticks_ms(), self.now)>= 1000:    
                self.now = utime.ticks_ms()
                ##@brief 2 byte temperature reading from temperature sensor
                self.temp = self.i2c.mem_read(2, addr= self.address, memaddr = 0b00000101) #0x18
                ##@brief upperbyte(most significant) of the 2 byte temperature reading
                self.UpperByte = self.temp[0]
                ##@brief lowerbyte(least signficant) of the 2 byte temperature reading
                self.LowerByte = self.temp[1]
                self.UpperByte = self.UpperByte & 0x1F
                if ((self.UpperByte & 0x10) == 0x10):
                    self.UpperByte = self.UpperByte & 0x0F
                    self.Temperature[0] = 256 - (self.UpperByte * 16 + self.LowerByte / 16)
                    self.Temperature[1] = 'C'
                else:
                    self.Temperature[0] = self.UpperByte * 16 + self.LowerByte / 16
                    self.Temperature[1] = 'C'
                print('Ambient temperature: '+ str(self.Temperature[0]) + '[C]')
                yield(self.Temperature[0])
            
        
    def check(self):
        '''@brief Checks if sensor is attached at the given bus address
        '''
        ##@brief list of viable addresses that the I2C master sees
        self.addr = self.i2c.scan()
        if self.address in self.addr:
            print('The sensor is properly attached.')
        else:
            print('Something is wrong with the attachment, check sensor wiring.')        

    def celsius(self):
        '''@brief Returns measured temperature in degrees Celsius
        '''
        if self.Temperature[1] == 'C':
            print('Ambient temperature: '+ str(self.Temperature[0]) + '[C]')
            return(self.Temperature[0])
        elif self.Temperature[1] == 'F':
            self.Temperature[0] = (self.Temperature[0] - 32)*5/9
            self.Temperature[1] = 'F'
            print('Ambient temperature: '+ str(self.Temperature[0]) + '[C]')
            return(self.Temperature[0])

    def fahrenheit(self):
        '''@brief Returns measured temperature in degrees Fahrenheit
        '''
        if self.Temperature[1] == 'C':
            self.Temperature[0] = self.Temperature[0] * 5/9 + 32
            self.Temperature[1] = 'C'
            print('Ambient temperature: '+ str(self.Temperature[0]) + '[F]')
            return(self.Temperature[0])
        elif self.Temperature[1] == 'F':
            print('Ambient temperature: '+ str(self.Temperature[0]) + '[F]')
            return(self.Temperature[0])
        
          

if __name__ == '__main__':
    
    __adcall__ = ADCAll(12, 0x70000)
    __adcall__.read_core_vref()
    __i2c__ = I2C(1, I2C.MASTER)

    __sensor__ = mcp9808(__i2c__,0b0011000)
    __amb__ = __sensor__.read_ambient()
    
    try:
        while True:
            
            ##  The amount of time in microseconds between runs of the task
            self.interval = 1000
            ## The timestamp for the first iteration
            self.start_time = utime.ticks_ms()
        
            ## Time at which the task should run next
            self.next_time = utime.ticks_add(self.start_time, self.interval)
            
            with open ("tempdata.csv","w") as a_file:
            a_file.write ("Time[sec],Nucleo Temperature[C],Ambient (MSP9808) Temperature [C]\r")
            # Add temperatures of Nucleo Board and mcp9808 to list every 60 sec.
            while(True):
                try:
                    self.curr_time = utime.ticks_ms()
                    if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                        time = utime.ticks_diff(self.curr_time,self.start_time)/1000
                        a_file.write ("{t},{i},{a}\r".format (t=time, i=internal_temp, a=ambient_temp))
                        __time__ = utime.ticks_diff(self.curr_time,self.start_time)/1000
                        __sensor__ = mcp9808.celsius()
                        __amb__ = TaskInternalTemp.celsius()
                        a_file.write ("{t},{i},{a}\r".format (t=__time__, i=__internal_temp__, a=__ambient_temp__))
                        ## Specifying the next time the task will run
                        self.next_time = utime.ticks_add(self.next_time, self.interval)
                except KeyboardInterrupt:
                    break
            print ("The file has by now automatically been closed.")  
            next(__amb__)
    except KeyboardInterrupt:
        pass
            
        