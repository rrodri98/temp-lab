## \page page_LabFour Lab 0x04: Temperature Data Collection 
#
#  In this lab, the temperature of the microcontroller as well as the ambient temperature was collected over an 8 hour time period. The ambient temperature
#  was collected using an I2C sensor. An I2C sensor carries data through the use of two connections: SDA for serial data and SCL for the serial clock. Each I2C device is
#  assigned a 7-bit address typically already set, but can be reprogrammed if the device has address control pins. To initate data collection, the 7-bit address is sent with the eighth bit
#  specifying whether to read or write data. Two bytes are read from the MCP9808 I2C temperature sensor and the bytes are converted to an integer value in degrees Celsius. Conversely, the
#  temperature of the NUCLEO is obtained using an analog-to-digital converter. The code implemented in this lab was ME405_lab4.py which contains the written class files that acted as temperature sensor drivers, and ME405_lab4main.py which 
#  utilized the drivers to collect temperature data over an 8 hour period and export it to a .CSV file. 
#
#  This lab was completed in groups. Both partners in this lab, Aaron Tran and Rebecca Rodriguez, collaborated on the code for the DAQ and the documentation required. Each member's contibution is clearly visible in the commit history for the respository used in Figure 1:
#  @image html commit_hist.png "Figure 1. Commit history of shared respository"
# 
#  Additionally, both members indpendently collected their own data for this lab. Aaron's data was taken next to his laptop from 7 PM on May 12, 2021 to 3 AM on May 13, 2021. 
#  Rebecca's data was taken also taken on May 12, 2021 over a shorter timespan. A plot of Aaron's results can be found in Figure 2, and a plot of Rebecca's results can be found in Figure 3.
#
#  @image html aaron_plot.png "Figure 2. Plot of Aaron's data, for raw data refer to https://bitbucket.org/rrodri98/temp-lab/src/master/aaron_data.csv"
#  @image html Plot_by_Rebecca.png "Figure 3. Plot of Rebecca's data, for raw data refer to https://bitbucket.org/rrodri98/temp-lab/src/master/Rebecca's%20temp%20data.csv"