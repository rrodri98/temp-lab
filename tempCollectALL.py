# -*- coding: utf-8 -*-
'''
@file main.py
@brief This script measures the internal temperature of the Nucleo-L47RG and ambient temperature from the MCP9808 and saves it to a file.
@author Justin Slavick and Rebecca Rodriguez
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date February 7, 2021
'''
import pyb
from pyb import I2C
import utime

class TaskAmbientTemp:
    '''
    This class establishes I2C communication between the temperature module and microcontroller.
    '''

	## Constructor for encoder task
	#
	#  Detailed info on encoder task constructor
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_GET_UPDATE  = 1  
    
    def __init__(self, i2c1, address):
        '''
        Creates an ambient temperature task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        
        # Create a buffer for data storage
        self.ambData = bytearray()
        
        self.i2c1 = i2c1
        
        self.address = address
        
        self.tempC = 0
        
        # Check for start condition ()
        pass
    
    def check(self):
        '''
        Verifies the sensor is attached at the same bus address according to the value in the manufacturer ID register.
        @param ______.
        '''
        # Scans all I2C addresses from 0x01 to 0x7f & returns a list of those that respond.
        ID_list = self.i2c1.mem_read (2, self.address, 0x06)   # Read sensor data
        ## From power-on values in mcp9808 data sheet:
        return (ID_list[0] == 0 and ID_list[1] == 84)
    
    def decodeTemp(self, tempBytes):
        '''
        

        Parameters
        ----------
        tempBytes : TYPE
            DESCRIPTION.

        Returns
        -------
        temp : TYPE
            DESCRIPTION.

        '''
        ## The Breakout Board returns a byte array with two indices. The first index represents the most significant byte while the second represents the least significant byte
        MSB = tempBytes[0]
        LSB = tempBytes[1]
        
        ## Initial temperature is set to 0. i represents the bit position and is also set to 0
        temp = 0
        i = 0
        ## While loop will keep right bit shifting LSB until it runs out of ones
        while (LSB != 0):
            ## The right-most bit is compared with a binary value of 1. That value of one or zero is multiplied with the resolution of that bit and added to the total temperature
            temp += (LSB & 0b1)*(2**(i-4))
            LSB = LSB>>1
            i += 1
        
        ## The bit counter is reset and the process is repeated with the MSB, until the bit representing the sign is reached
        i = 0
        while (i <= 3):
            temp += (MSB & 0b1)*(2**(i+4))
            MSB = MSB>>1
            i += 1
            
        ## If the bit representing the sign is true, the value of temp is multiplied by -1.
        if(MSB>>1 == 1):
            temp *= -1
            
        return temp
    
    def celsius(self):
        '''
        Returns measured temp in celsius.
        @param mcpTemp Represents the ambient temperature recorded by the MCP9808 module.
        '''
        ## Needs if statements to decode output to decimal form
        
        
        tempBytes = self.i2c1.mem_read (2, self.address, 0x05)   # Read sensor data
        self.tempC = self.decodeTemp(tempBytes)
        return self.tempC
    
    def fahrenheit(self, tempC):
        '''
        Returns measured temp in fahrenheit.
        @param mcpTemp Represents the ambient temperature recorded by the MCP9808 module.
        '''
        # Convert Celsius temp to Fahrenheit
        return ((tempC * (9 / 5)) + 32)

## In order to separate the task of reading the internal temperature with the microcontroller with the task of writing the data to the CSV file, this class is created to do the later.
class TempWriter:
    
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_GET_INTERNAL_TEMP  = 1  
    
    def __init__(self, interval, end_time):
        '''
        

        Parameters
        ----------
        interval : TYPE
            DESCRIPTION.
        uart : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration / Time in microseconds
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.end_time = end_time
        
        ## Constructs an I2C object to be used by the Nucleo as a Master
        self.i2c1 = pyb.I2C(1, pyb.I2C.MASTER, baudrate = 100000)
        
        ## Constructs a TaskAmbientTemp object which recieves the I2C object and its address. This allows the ambient temperature from the mcp9808 device to be measured.
        self.mcp_temp = TaskAmbientTemp(self.i2c1, 24)
        
        ## Constructs a TaskInternalTemp object that allows the internal temperature of the microcontroller to be measured.
        self.nucleo_temp = TaskInternalTemp()
        
        self.ambient_temp_list = []
        self.internal_temp_list = []
        
    def run(self):
        '''
        @param ______.
        '''
        
        with open ("tempdata.csv","w") as a_file:
            a_file.write ("Time(s),STM32 Temperature(C),Ambient (MSP9808) Temperature\r")
            # Add temperatures of Nucleo Board and mcp9808 to list every 60 sec.
            while(True):
                try:
                    self.curr_time = utime.ticks_ms()
                    if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                        time = utime.ticks_diff(self.curr_time,self.start_time)/1000
                        internal_temp = self.nucleo_temp.celsius()
                        ambient_temp = self.mcp_temp.celsius()
                        a_file.write ("{t},{i},{a}\r".format (t=time, i=internal_temp, a=ambient_temp))
                        ## Specifying the next time the task will run
                        self.next_time = utime.ticks_add(self.next_time, self.interval)
                except KeyboardInterrupt:
                    break
            print ("The file has by now automatically been closed.")

class TaskInternalTemp:
    '''
    
    '''
    
    def __init__(self):
        '''
        Creates an internal temperature task object.
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        
        self.intData = []
        self.adcall = pyb.ADCAll(12, 0x70000) # 12 bit resolution, internal channels
        self.temp = 0;
        
        pass
        
    def celsius(self):
        self.adcall.read_vref()
        self.temp = self.adcall.read_core_temp()
        return self.temp
    
    def fahrenheit(self):
        self.temp = self.adcall.read_core_temp()
        return self.temp * (9/5) + 32
       
if __name__ == "__main__":
    write = TempWriter(1000,30000)
    write.run()