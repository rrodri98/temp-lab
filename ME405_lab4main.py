# -*- coding: utf-8 -*-
'''@file        ME405_lab4main.py
@brief          Calls classes from ME405_lab4.py to collect data on Nucleo temperature and ambient temperature
@details        For source code refer to https://bitbucket.org/rrodri98/temp-lab/src/master/ME405_lab4main.py
@author         Aaron Tran
@author         Rebecca Rodriguez
@date           5/11/21
'''

from ME405_lab4 import TaskInternalTemp
from ME405_lab4 import mcp9808
from pyb import I2C
from array import array
import utime


#Setup arrays and instantiate class objects
##@brief Initial start time of machine
start_time = utime.ticks_ms()
##@brief Periodic timer
next_time = utime.ticks_ms()
__i2c__ = I2C(1, I2C.MASTER)
__sensor__ = mcp9808(__i2c__,0b0011000)
__amb__ = __sensor__
__nuc__ = TaskInternalTemp()    
##@brief array containing time data
times = array('f', 700*[0])
##@brief array containing ambient temperature data
amb_temps = array('f', 700*[0])
##@brief array contianing core temperature
core_temps = array('f', 700*[0])
print('Arrays generated')
__n__=0
try:
    while True:
        #Collect data once every minute
        curr_time = utime.ticks_ms()
        if utime.ticks_diff(curr_time, next_time) >= 60000:
            next_time = utime.ticks_ms()
            time = utime.ticks_diff(curr_time,start_time)/1000
            times[__n__] = time
            print('At time t = ' + str(time) + '[s]:')
            amb_temps[__n__] = __amb__.read_ambient()
            core_temps[__n__] = __nuc__.celsius()
            __n__+=1
except KeyboardInterrupt:
    #Upon pressing Ctrl+C, write to csv
    print('Starting to write to tempdata.csv')
    with open ("tempdata.csv","w") as a_file:
        print('Writing to file tempdata.csv')
        a_file.write ("Time[sec],Nucleo Temperature[C],Ambient (MSP9808) Temperature [C]\r")
        for n in range(700):   
            print("{t},{i},{a}\r".format (t=times[n], i=core_temps[n], a=amb_temps[n]))
            a_file.write ("{t},{i},{a}\r".format (t=times[n], i=core_temps[n], a=amb_temps[n]))
    print("The file has by now automatically been closed.")  