# -*- coding: utf-8 -*-
'''@file        ME405_lab4.py
@brief          Contains class mcp9808 class and methods
@author         Aaron Tran
@author         Rebecca Rodriguez
@date           5/11/21
'''
from pyb import ADCAll
from pyb import I2C
from array import array
import utime
import micropython


micropython.alloc_emergency_exception_buf(200)
#SLAVE ADDRESS: 0b0011000

class TaskInternalTemp:
    '''
    Reads core temperature of Nucleo microcontroller. Contains methods for taking the reading in celsius or fahrenheit
    '''
    
    def __init__(self):
        '''
        Creates an internal temperature task object.
        '''
        ##@brief ADC object containing, which can retrieve temperature readings
        self.adcall = ADCAll(12, 0x70000) # 12 bit resolution, internal channels
        ##@brief Temperature reading from Nucleo
        self.temp = 0;
        pass
        
    def celsius(self):
        '''
        Returns internal temperature in degrees Celsius
        '''
        self.adcall.read_vref()
        self.temp = self.adcall.read_core_temp()
        print('Core temperature: ' + str(self.temp) + '[C]')
        return self.temp
    
    def fahrenheit(self):
        '''
        Returns internal temperature in degrees Fahrenheit
        '''
        self.temp = self.adcall.read_core_temp()
        return self.temp * (9/5) + 32     

class mcp9808:
    '''@brief Reads temperature data from mcp9808 sensor
    @details Handles I2C communication between microcontroller and mcp9808. Contains methods for reading ambient temperature, checking if the sensor is properly attached,
             and converting temperature readings to rather degrees fahrenheit or degrees celsius
    '''
    def __init__(self, i2c, address):
        '''@brief Initializes class
        @param i2c an object of the I2C, should be configured as MASTER along with whatever SLA/SCL bus is necessary
        @param address 7-bit address of I2C bus
        '''
        ##@brief class attribute of the i2c parameter
        self.i2c = i2c
        ##@brief class attribute of the address parameter
        self.address = address
                
    def read_ambient(self):
        '''@brief Retrieves ambient temperature readings from the sensor in degrees Celsius
        '''
        ##@brief Temperature vector, where first position is the numerical value, and the second position is the units (C or F)
        self.Temperature = [0,0]
        ##@brief 2 byte temperature reading from temperature sensor
        self.temp = self.i2c.mem_read(2, addr= self.address, memaddr = 0b00000101) #0x18
        ##@brief upperbyte(most significant) of the 2 byte temperature reading
        self.UpperByte = self.temp[0]
        ##@brief lowerbyte(least signficant) of the 2 byte temperature reading
        self.LowerByte = self.temp[1]
        #Apply mask to clear flag bits
        self.UpperByte = self.UpperByte & 0x1F
        #Checks for signed bit and calculates temperature accordingly
        if ((self.UpperByte & 0x10) == 0x10):
            #Negative signed bit
            self.UpperByte = self.UpperByte & 0x0F
            self.Temperature[0] = 256 - (self.UpperByte * 16 + self.LowerByte / 16)
            self.Temperature[1] = 'C'
        else:
            #Positive signed bit
            self.Temperature[0] = self.UpperByte * 16 + self.LowerByte / 16
            self.Temperature[1] = 'C'
        print('Ambient temperature: '+ str(self.Temperature[0]) + '[C]')        
        return(self.Temperature[0])

            
        
    def check(self):
        '''@brief Checks if sensor is attached at the given bus address
        '''
        ##@brief list of viable addresses that the I2C master sees
        self.addr = self.i2c.scan()
        if self.address in self.addr:
            print('The sensor is properly attached.')
        else:
            print('Something is wrong with the attachment, check sensor wiring.')        

    def celsius(self):
        '''@brief Returns measured temperature in degrees Celsius
        '''
        if self.Temperature[1] == 'C':
            print('Ambient temperature: '+ str(self.Temperature[0]) + '[C]')
            return(self.Temperature[0])
        elif self.Temperature[1] == 'F':
            self.Temperature[0] = (self.Temperature[0] - 32)*5/9
            self.Temperature[1] = 'F'
            print('Ambient temperature: '+ str(self.Temperature[0]) + '[C]')
            return(self.Temperature[0])

    def fahrenheit(self):
        '''@brief Returns measured temperature in degrees Fahrenheit
        '''
        if self.Temperature[1] == 'C':
            self.Temperature[0] = self.Temperature[0] * 5/9 + 32
            self.Temperature[1] = 'C'
            print('Ambient temperature: '+ str(self.Temperature[0]) + '[F]')
            return(self.Temperature[0])
        elif self.Temperature[1] == 'F':
            print('Ambient temperature: '+ str(self.Temperature[0]) + '[F]')
            return(self.Temperature[0])
        
          

if __name__ == '__main__':
    ##@brief Initial start time of machine
    start_time = utime.ticks_ms()
    ##@brief Periodic timer
    next_time = utime.ticks_ms()
    __i2c__ = I2C(1, I2C.MASTER)

    __sensor__ = mcp9808(__i2c__,0b0011000)
    __amb__ = __sensor__
    
    __nuc__ = TaskInternalTemp()
    
    ##@brief array containing time data
    times = array('f', 700*[0])
    ##@brief array containing ambient temperature data
    amb_temps = array('f', 700*[0])
    ##@brief array contianing core temperature
    core_temps = array('f', 700*[0])
    print('Arrays generated')
    __n__=0
    try:
        while True:
            curr_time = utime.ticks_ms()
            if utime.ticks_diff(curr_time, next_time) >= 60000:
                next_time = utime.ticks_ms()
                time = utime.ticks_diff(curr_time,start_time)/1000
                times[__n__] = time
                print('At time t = ' + str(time) + '[s]:')
                amb_temps[__n__] = __amb__.read_ambient()
                core_temps[__n__] = __nuc__.celsius()
                __n__+=1
    except KeyboardInterrupt:
        print('Starting to write to tempdata.csv')
        with open ("tempdata.csv","w") as a_file:
            print('Writing to file tempdata.csv')
            a_file.write ("Time[sec],Nucleo Temperature[C],Ambient (MSP9808) Temperature [C]\r")
            for n in range(700):   
                print("{t},{i},{a}\r".format (t=times[n], i=core_temps[n], a=amb_temps[n]))
                a_file.write ("{t},{i},{a}\r".format (t=times[n], i=core_temps[n], a=amb_temps[n]))
        print("The file has by now automatically been closed.")    
